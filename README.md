# README #

### What is this repository for? ###

* This repository is to conduct loadflow analysis on Octave Platform.

### How do I get set up? ###

*This package requires io package to be installed on octave.

*Everytime, io package has to be loaded,

*Run the mail file Powerflow.m

*Choose required options from the pop-up window,

*Results will be displayed on the octave command window, stored in Excel shell and text document.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: R S Ananda Murthy, Associate professor, SJCE, Mysore.
Mail id: rsamurti@gmail.com