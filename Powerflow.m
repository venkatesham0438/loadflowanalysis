% Function to perform Gauss Siedel method of Load Flow
% Authors: Manikanta.N.R, Neeraj, Varsha.T.P and Venkatesh.A.M
% 8th Sem(2012-2016), under the guidance of
% R. S. Ananda Murthy, Associate Professor, Dept. of EEE, SJCE, Mysore-06

clc;
prob=menu("Select problem to be solved","3-bus","5-bus","14-bus","Stevenson_5_bus");
switch prob
   case 1
    num=3;
    linedata = xlsread('Linedata_venky.xlsx','Linedata3','A2:F4'); % Reads linedata
    busdata =xlsread('Linedata_venky.xlsx','Busdata3','A2:J4');    % Reads busdata
    ybus=Ybus_venky(num,linedata);  % Calling Ybus function
   case 2
    num=5;
    linedata=xlsread('Linedata_venky.xlsx','Linedata5','A2:F8');  % Reads linedata
    busdata =xlsread('Linedata_venky.xlsx','Busdata5','A2:J6');   % Reads busdata
    ybus=Ybus_venky(num,linedata);   % Calling Ybus function
   case 3
    num=14;
    linedata=xlsread('Linedata_venky.xlsx','Linedata14','A2:F21');  % Reads linedata
    busdata =xlsread('Linedata_venky.xlsx','Busdata14','A2:J15');   % Reads busdata
    ybus=Ybus_venky(num,linedata);    
   case 4
    num=5;
    linedata=xlsread('Linedata_venky.xlsx','Steve_Line','A2:F8'); % Reads linedata
    busdata =xlsread('Linedata_venky.xlsx','Steve_Bus','A2:J6');  % Reads busdata
    ybus=Ybus_venky(num,linedata);  % Calling Ybus function
endswitch 

tolerance=input("Tolerance:");   % Reads tolerance

 Gauss_siedel(num,ybus,tolerance,linedata,busdata);    % Calling Gauss Siedel function
 